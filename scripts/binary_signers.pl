#! /usr/bin/perl

use strict;

use DBI;
use JSON;

my $dbh_pb = DBI->connect("dbi:Pg:service=projectb");

my $qpb = "
SELECT
	binaries.package,
	binaries.version,
	architecture.arch_string,
	uid.uid
FROM
	binaries,
	architecture,
	uid,
	fingerprint

WHERE
	    architecture=architecture.id
	AND binaries.sig_fpr=fingerprint.id
	AND fingerprint.uid=uid.id
	AND binaries.id IN (
		SELECT
			bin
		FROM
			bin_associations
		where
			suite IN (
				SELECT
					id
				FROM
					suite
				WHERE
					suite_name IN ('unstable','testing-proposed-updates','proposed-updates','oldstable-proposed-updates')
			)
	)
;

";

my $binaries = {};

my $sth_pb = $dbh_pb->prepare($qpb);
$sth_pb->execute;
while (my $row = $sth_pb->fetchrow_hashref()) {
	my $is_buildd = JSON::false;
	if ($row->{"uid"} =~ m/^buildd_.*\@buildd.debian.org$/) {
		$is_buildd = JSON::true;
	}
	my $bininfo = {};
	$bininfo->{"uid"} = $row->{"uid"};
	$bininfo->{"buildd"} = $is_buildd;
	$binaries->{$row->{"package"}}->{$row->{"version"}}->{$row->{"arch_string"}} = $bininfo;
}

print to_json($binaries,{pretty => 1, canonical => 1});

